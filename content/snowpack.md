+++
title = "🇬🇧 Snowpack v2: the new way to develop a frontend app ?"
date = 2020-05-06
draft = false

[taxonomies]
categories = ["English"]
tags = ["tools", "javascript"]

[extra]
lang = "fr"
toc = true
show_comment = true
math = false
mermaid = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120
+++

Yesterday the 5 of May 2020 I tried the tool [snowpack](https://www.snowpack.dev/) which give us the promise to lighten our development boilerplate and speed up the "bundling phase" in hot reload when you are creating an application with JS technologies.

The traditional way is to rely on webpack that builds a bundle javascript file involving all the depencies external or internal. This leads to a big file that must be rebuild at each modification in sources.

Snowpack takes the side of using modern browser capabilities like natives modules. Here an interesting [article](https://pawelgrzybek.com/native-ecmascript-modules-in-the-browser/) on the subject. Long story short it allows the browser to download a module when it's needed. The idea is therefore to speed up the hot reloading by not bundling at all your external dependencies, in fact by building nothing 🙂. 

You'll better understand all of this with an example.

But first, let's install this !

## Requirements
Snowpack is based on javascript, so you need node/npm environment installed.

There is another restriction, it works only on [modern browsers](https://caniuse.com/#search=modules).

## Installation
It's dead simple

```bash
yarn add -D snowpack
```

That's all 😃

### Preset
Snowpak comes with a bootstrap tool called `create-snowpack-app` it creates a new project with a preset. As I write this lines, the tool is not totally stable.

The simplest preset is `blank` it allows to create a simple javascript app without any popular library.

```bash
npx create-snowpack-app my-project --template @snowpack/app-template-blank --use-yarn
```

There is a bunch of presets that can be found with [`csa-template`](https://www.npmjs.com/search?q=keywords%3Acsa-template) tag.


After packages installation, if you see this :

```
[...]
$ snowpack
✔ snowpack install complete. [0.26s]

  ⦿ web_modules/           size       gzip       brotli
    └─ canvas-confetti.js  15.98 KB   4.44 KB    3.87 KB


✨  Done in 20.69s.

  - Initializing git repo.

  - Success!

Quickstart:

  cd my-project
  yarn start

All Commands:

  yarn install
  Install all dependencies (npm + snowpack).
  We already ran this one for you.
  yarn start
  Starts the development server.
  yarn build
  Bundles the app into static files for production.
  yarn test
  Starts the test runner.
```

It indicates that all is ready to dev 🤩

## Develop your app with snowpack

Here is the most interesting part, first start the dev server.

```bash
cd my-project && yarn start
```

It will automatically open a web browser. You'll see some confetties pop in. That's done by the example `index.js` given by the preset.

Let's look upon development console, in the network tab you've the "waterfall" of the files donwloaded.

![](../assets/images/snowpack/waterfall1.png)

As you can there is no bundle file, just an `index.js` downloaded, shortly followed by a `canvas-confetti.js`.

To better understand what happened. Take a look on the `index.js`.

```
import confetti from "canvas-confetti";

[...]
```
_I have willingly truncate not important parts_

That's here the magic comes, this line which in a webpack environement should bunldle the entire `canvas-confetti` lib has totally different usage with snowpack. It tells to the system to make `canvas-confetti` a module that can be downloaded separately by the browser.

This leads to the creation of the `canvas-confetti.js` file previously seen in the waterfall.

### How does it work ?
For the ones who like me want to understand everything before using a tool, here is a short explanation.

If you open the `package.json` you can see this :

```json
{
  "webDependencies": {
    "canvas-confetti": "^1.2.0"
  },
  "devDependencies": {
    "parcel-bundler": "^1.12.4",
    "snowpack": "^2.0.0-0"
  }
}
```

There is a special key called `webDependencies`, it acts like `devDependencies` or `dependencies`. And defines a package dependency and a version constraint.

{% tip(header="Help" %}
As I write this lines, the `snowpack add` doesn't seem to work. So I'll only use classic package management to the rest of the article.
{% end %}

There is also a `web_modules` folder where resides our `canvas-confetti.js` file. Plus another json file called `import-map.json` it maps package name dans path to find them.

So every time you add a new module by importing it in any source file having a root the `index.js` a new file will be created in this folder.

For example let's say that you need `lodash`.

First in the `index.js`, add this line.

```js
import * as _ from 'lodash'
```

Snowpack will complain

```
Snowpack

  http://localhost:3000 > http://192.168.1.18:3000
  Server started in 11ms.

  mount:web_modules.......[DONE]
  mount:public............[DONE]
  build:js................[READY]

▼ Snowpack

  Package lodash could not be found!
    1. Make sure that your web_modules/ directory is mounted correctly.
    2. Add lodash to your Snowpack "webDependencies".
```

Then install lodash with yarn:


```
yarn add -D lodash
```

Snowpack will build the lodash module, one time forever.

```
$ snowpack
✔ snowpack install complete. [2.00s]

  ⦿ web_modules/           size       gzip       brotli
    ├─ canvas-confetti.js  15.98 KB   4.44 KB    3.87 KB
    └─ lodash.js           540.42 KB  96.25 KB   73.71 KB


✨  Done in 5.56s.
```

If you relaunch the `yarn start` or make a modification in `index.js` file. The waterfall must have changed.

![](../assets/images/snowpack/waterfall2.png)

You can see now not 2 files but three, the `index.js`, the good old `canvas-confetti.js` and the last coming `lodash.js`.

A quick look into the `web_modules` shows us that the `lodash.js` create by the `yarn add` command is here.

## Treeshaking like a boss
Ok, but what about importing my own modules?

It's the same thing, create a new file in your project. let's call it `module1.js`, leave it empty for now.

Add this line into `index.js`
```js
import './module1.js';
```

And hop

![](../assets/images/snowpack/waterfall3.png)

Our `module1.js` is append to the modules downloaded after `index.js`

Let's play a game

![](https://media.giphy.com/media/vP4ci3sz5YZHi/giphy.gif)

Here is an import tree

![](../assets/images/snowpack/tree.png)

Let's create the files:

```bash
echo "import './module1-1.js'\nimport './module1-2.js'" > module1.js
```

```bash
echo "import './module2-1.js'" > module2.js
```

```bash
echo module1-1.js module1-2.js module2-1.js | xargs -n1 -I {} bash -c "echo \"console.log('{}')\" > {}"
```

Add add this line to `index.js`

```js
import './module2.js'
```

![](../assets/images/snowpack/waterfall4.png)

And yes the modules are dowloaded in the correct order even nested. 🎉

### Statics
{% detail(header="Work in progress") %}
I haven't found out how this work yet 😢
{% end %}

## Build the production dist
Snowpack provides a command to achieve this:
```
yarn build
```

this command does this
```
snowpack build --bundle
```

Which leads to create a `dist` folder.

But if you run
```
cd build && http-server
```

![](../assets/images/snowpack/console.png)

Hum 🥺 The project is still in beta. I guess all will work in the next weeks / months.

# Conclusion
Really promising but too fresh to be use in in lieu  of parcel or webpack. Using native modules is a really clever idea, and I can't wait where this will lead us 😄.

If there is any news about snowpack I could update this post.

I hope you enjoy read this article as I took pleasure to write it ❤️.

See you soon for the next adventures !
